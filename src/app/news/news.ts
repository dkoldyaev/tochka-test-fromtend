import moment = require("moment");
import {IEvent} from "../event-list/event.interface";

export interface INews extends IEvent {
  id: string;
  title: string;
  text: string;
  date: Date|string;
  type: string;
}

export class News implements INews {

  public id: string;
  public title: string;
  public text: string;
  public date: Date;
  public type: string = 'news';

  public constructor(data: INews) {

    for (let field_name in data) {

      let field_value = data[field_name];
      if (field_name == 'date') {
        field_value = moment(field_value, 'YYYY-MM-DD HH:mm:ss').toDate()
      }
      //TODO: Переделать генерацию "рыбы", чтобы сразу по параграфам выдавались данные
      if (field_name == 'text') {
        field_value = '<p>'+field_value+'</p>';
      }
      this[field_name] = field_value;

    }

  }

}
