import {Component, Input, OnInit} from '@angular/core';
import {News} from "../news";
import {AbstractDynamicComponent} from "../../event-list/event-list-item/dynamic-module/dynamic-component";
import {NewsService} from "../news.service";
import {Observable} from "rxjs/Observable";
import {EventListEventService} from "../../event-list/event-list-event.service";

@Component({
  selector: 'app-news-list-item',
  templateUrl: './news-list-item.component.html',
  styleUrls: [
    './news-list-item.component.less',
  ],
  providers: [
    NewsService,
    EventListEventService,
  ]
})
export class NewsListItemComponent extends AbstractDynamicComponent {

  @Input()
  event: News;

  constructor(
    protected service: NewsService,
    protected event_list_service: EventListEventService) {
    super(service);
  }

  ngOnInit() {
  }

  delete() {
    this.service.delete(this.event.id).subscribe(data => {
      this.deleted.next(true);
    });
  }

}
