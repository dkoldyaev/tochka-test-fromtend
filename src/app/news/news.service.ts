import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {TransferHttp} from "../../modules/transfer-http/transfer-http";
import {Observable} from "rxjs/Observable";
import {INews, News} from "./news";

@Injectable()
export class NewsService {

  private base_api_host = '/news';
  private get_list_url(): string {
    return environment.api_host + this.base_api_host + '/';
  }
  private get_detail_url(id: string): string {
    return environment.api_host + this.base_api_host + '/' + id;
  }

  constructor(private http: TransferHttp) {}

  public getList(): Observable<INews[]> {
    return this.http.get(this.get_list_url());
  }

  public getDetail(id): Observable<INews> {
    return this.http.get(this.get_detail_url(id));
  }

  public delete(id): Observable<any> {
    return this.http.delete(this.get_detail_url(id));
  }

}
