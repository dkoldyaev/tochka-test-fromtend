import { TestBed, inject } from '@angular/core/testing';

import { EventListEventService } from './event-list-event.service';

describe('EventListEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventListEventService]
    });
  });

  it('should be created', inject([EventListEventService], (service: EventListEventService) => {
    expect(service).toBeTruthy();
  }));
});
