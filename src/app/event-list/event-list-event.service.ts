import {EventEmitter, Injectable} from '@angular/core';
import {IEvent} from "./event.interface";

@Injectable()
export class EventListEventService {

  deleteEvent: EventEmitter<IEvent> = new EventEmitter<IEvent>();
  onAdded: EventEmitter<IEvent> =   new EventEmitter<IEvent>();

}
