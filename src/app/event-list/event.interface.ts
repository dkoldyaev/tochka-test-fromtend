export interface IEvent {

  id: string;
  date: Date|string;
  readonly type: string;

}
