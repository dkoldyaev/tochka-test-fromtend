import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NewsService} from "../../news/news.service";
import {TransactionsService} from "../../transactions/transactions.service";
import {EventListItem} from "../event-list-item/event-list-item";
import {INews, News} from "../../news/news";
import {Transaction} from "../../transactions/transaction";
import {IEvent} from "../event.interface";
import {EventAddFormComponent} from "../event-add-form/event-add-form.component";
import {EventListEventService} from "../event-list-event.service";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.less'],
  providers: [
    NewsService,
    TransactionsService,
    EventListEventService,
  ]
})
export class EventListComponent implements OnInit {

  events: EventListItem[] = [];

  constructor(
    private transaction_service: TransactionsService,
    private news_service: NewsService,
    private event_list_service: EventListEventService,
  ) {}

  ngOnInit() {
    this.transaction_service.getList().subscribe((data) => {
      for (let i in data) {
        let new_transaction = new EventListItem(new Transaction(data[i]));
        this.events.push(new_transaction);
      }
    });
    this.news_service.getList().subscribe((data) => {
      for (let i in data) {
        let new_news = new EventListItem(new News(data[i]));
        this.events.push(new_news);
      }
    });
    this.event_list_service.deleteEvent.subscribe((event: IEvent) => {
      console.log(event, '1111212');
    });
    console.log('ngOnInit', this.event_list_service.deleteEvent);
  }

  addEventToList(event: IEvent) {
    if (event instanceof News) {
      this.events.push(new EventListItem(new News(event)));
    } else if (event instanceof Transaction) {
      this.events.push(new EventListItem(new Transaction(event)));
    }
  }

}
