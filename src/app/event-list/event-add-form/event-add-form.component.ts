import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IEvent} from "../event.interface";

@Component({
  selector: 'app-event-add-form',
  templateUrl: './event-add-form.component.html',
  styleUrls: ['./event-add-form.component.less']
})
export class EventAddFormComponent implements OnInit {

  @Output()
  addEventToList: EventEmitter<IEvent> = new EventEmitter<IEvent>();

  addEvent(event_data: IEvent) {
    this.addEventToList.emit(event_data);
  }

  constructor() { }

  ngOnInit() {
  }

}
