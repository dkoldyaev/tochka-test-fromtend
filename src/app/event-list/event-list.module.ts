import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventListComponent } from './event-list/event-list.component';
import { EventListItemComponent } from './event-list-item/event-list-item.component';
import {RouterModule, Routes} from "@angular/router";
import { EventAddFormComponent } from './event-add-form/event-add-form.component';
import {DynamicModule} from "./event-list-item/dynamic-module/dynamic.module";
import {DynamicComponent} from "./event-list-item/dynamic-module/dynamic/dynamic.component";

export const eventListRoute: Routes = [
  { path: '', component: EventListComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(eventListRoute),
  ],
  declarations: [
    DynamicComponent,
    EventListComponent,
    EventListItemComponent,
    EventAddFormComponent,
  ]
})
export class EventListModule { }
