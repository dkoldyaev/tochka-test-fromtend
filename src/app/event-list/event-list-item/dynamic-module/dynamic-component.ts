import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {IEvent} from "../../event.interface";
import {Observable} from "rxjs/Observable";
import {EventListEventService} from "../../event-list-event.service";
import {Subject} from "rxjs/Subject";

export abstract class AbstractDynamicComponent implements OnInit {

  @Input()
  event: IEvent;

  protected deleted: Subject<boolean> = new Subject<boolean>();

  constructor(
    protected service
  ) {
  }

  ngOnInit() {
    throw new Error('Переопределите этот компонент');
  }

}
