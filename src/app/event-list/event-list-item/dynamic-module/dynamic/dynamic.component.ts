import {
  Compiler, Component, Input, ModuleWithComponentFactories, OnInit, ReflectiveInjector,
  ViewContainerRef
} from '@angular/core';
import {IEvent} from "../../../event.interface";
import {promise} from "selenium-webdriver";
import {DynamicModule} from "../dynamic.module";
import {NewsListItemComponent} from "../../../../news/news-list-item/news-list-item.component";
import {TransactionListItemComponent} from "../../../../transactions/transaction-list-item/transaction-list-item.component";
import {EventListEventService} from "../../../event-list-event.service";

export class ComponentData {
  component: any;
  data: IEvent;
}

@Component({
  selector: 'app-dynamic-list-item',
  template: ' ',
  styleUrls: ['./dynamic.component.less'],
  providers: [
    EventListEventService
  ]
})
export class DynamicComponent implements OnInit {

  @Input()
  event: IEvent;

  private factory: ModuleWithComponentFactories<DynamicModule>;

  public constructor(
    private vcRef: ViewContainerRef,
    private compiler: Compiler,
    private event_list_service: EventListEventService
  ) {}

  ngOnInit() {
    if (this.event) {
      this.renderEvent(this.event);
    }
  }

  renderEvent(event: IEvent) {
    this.vcRef.clear();
    this.compiler.compileModuleAndAllComponentsAsync(DynamicModule)
      .then((moduleWithyComponentFactory: ModuleWithComponentFactories<DynamicModule>) => {
        this.factory = moduleWithyComponentFactory;
        let component = this.convertToComponent(event);
        this.insertListItem(component);
      });
  }

  private convertToComponent(event_data: IEvent) : ComponentData {
    switch (event_data.type) {
      case 'news' :
        return {
          component: NewsListItemComponent,
          data: event_data,
        };
      case 'transaction':
        return {
          component: TransactionListItemComponent,
          data: event_data,
        };
      default :
        throw new Error(`Нужно написать обработку для типа "${event_data.type}"`);
    }
  }

  private insertListItem(component_data: ComponentData) {
    const componentFactory = this.factory.componentFactories.find(
      x => x.componentType === component_data.component
    );
    // TODO: Переделать на современный инжектор
    const injector = ReflectiveInjector.fromResolvedProviders([], this.vcRef.parentInjector);
    const componentRef = this.vcRef.createComponent(componentFactory, this.vcRef.length, injector, []);
    componentRef.instance['event'] = component_data.data;
    componentRef.instance.deleted.subscribe((deleted: boolean) => {
      console.log(component_data.data, this.event_list_service.deleteEvent);
      this.event_list_service.deleteEvent.emit(component_data.data);
      this.vcRef.clear();
    })
  }

  public onHide($event) {
    this.vcRef.clear();
    console.log($event);
  }

}
