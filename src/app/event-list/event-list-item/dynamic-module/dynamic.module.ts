import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListItemComponent } from "../../../news/news-list-item/news-list-item.component";
import { TransactionListItemComponent } from "../../../transactions/transaction-list-item/transaction-list-item.component";

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    // DynamicComponent,
  ],
  declarations: [
    // DynamicComponent,
    NewsListItemComponent,
    TransactionListItemComponent,
  ],
})
export class DynamicModule { }
