import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {EventListItem} from "./event-list-item";
import {del} from "selenium-webdriver/http";
import {AbstractDynamicComponent} from "./dynamic-module/dynamic-component";
import {NewsListItemComponent} from "../../news/news-list-item/news-list-item.component";
import {TransactionListItemComponent} from "../../transactions/transaction-list-item/transaction-list-item.component";
import {DynamicComponent} from "./dynamic-module/dynamic/dynamic.component";
import {EventListEventService} from "../event-list-event.service";
import {IEvent} from "../event.interface";

@Component({
  selector: 'app-event-list-item',
  templateUrl: './event-list-item.component.html',
  styleUrls: ['./event-list-item.component.less'],
  providers: [EventListEventService]
})
export class EventListItemComponent implements OnInit {

  @Input()
  event_item: EventListItem;

  @ViewChild(AbstractDynamicComponent)
  private dynamic_item_component: AbstractDynamicComponent;

  constructor(
    protected event_list_service: EventListEventService
  ) { }

  ngOnInit() {
  }

  toggle() {
    this.event_item.collapsed = !this.event_item.collapsed;
  }

  remove(event: IEvent) {
    console.log(event, 'ololo!!');
    // this.event_list_service.deleteEvent.subscribe((event: IEvent) => {
    //   console.log(event, 'ololo!!');
    //   if (event.id != this.event_item.event.id) {
    //     return;
    //   }
    //   this.event_item = null;
    //   console.log('ololo!!');
    // });
    // TODO: Разобраться как достучаться до метода компонента, указывая только абстрактный компонент во ViewChild
    // console.log(this.dynamic_item_component);
    // this.dynamic_item_component.delete().subscribe((data) => {
    //   this.event_item = null;
    // });
  }

}
