import {ITransaction, Transaction} from "../../transactions/transaction";
import {INews, News} from "../../news/news";
import {IEvent} from "../event.interface";

export class EventListItem {

  public collapsed: boolean = true;
  public event: IEvent;
  public type: string = null;

  public constructor(event: IEvent) {
    this.event = event;
    this.type = event.type;
  }

}
