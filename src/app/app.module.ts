import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from "@angular/router";
import {environment} from "../environments/environment";
import {HttpModule} from "@angular/http";
import {TransferHttpModule} from "../modules/transfer-http/transfer-http.module";
import {BrowserTransferStateModule} from "../modules/transfer-state/browser-transfer-state.module";
import {EventListModule} from "./event-list/event-list.module";

export const appRoutes: Routes = [
  { path: '', loadChildren: './event-list/event-list.module#EventListModule' },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    TransferHttpModule,
    BrowserTransferStateModule,
    RouterModule.forRoot(
      appRoutes//,
      // { enableTracing: !environment.production }
    )
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
