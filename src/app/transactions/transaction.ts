import moment = require("moment");
import {IEvent} from "../event-list/event.interface";

export interface ITransaction extends IEvent{
  id:         string;
  amount:     number;
  currency:   "RUB"|"USD"|"EUR";
  sender:     string;
  direction:  "receipt"|"expense";
  date:       Date|string;
  description:string;
}

export class Transaction implements ITransaction {

  public id: string;
  public amount: number;
  public currency: "RUB"|"USD"|"EUR";
  public sender: string;
  public direction: "receipt"|"expense";
  public date: Date;
  public description: string;
  public type: string = 'transaction';

  public constructor(data: ITransaction) {

    for (let field_name in data) {

      let field_value = data[field_name];
      if (field_name == 'date') {
        field_value = moment(field_value, 'YYYY-MM-DD HH:mm:ss').toDate()
      }
      this[field_name] = field_value;

    }

  }

}
