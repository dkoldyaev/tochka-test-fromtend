import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Transaction} from "../transaction";
import {AbstractDynamicComponent} from "../../event-list/event-list-item/dynamic-module/dynamic-component";
import {TransactionsService} from "../transactions.service";
import {Observable} from "rxjs/Observable";
import {IEvent} from "../../event-list/event.interface";
import {EventListEventService} from "../../event-list/event-list-event.service";

@Component({
  selector: 'app-transaction-list-item',
  templateUrl: './transaction-list-item.component.html',
  styleUrls: [
    './transaction-list-item.component.less',
  ],
  providers: [
    TransactionsService,
    EventListEventService,
  ]
})
export class TransactionListItemComponent extends AbstractDynamicComponent {

  @Input()
  event: Transaction;

  constructor(
    protected service: TransactionsService,
    protected event_list_service: EventListEventService
  ) {
    super(service);
  }

  ngOnInit() {
  }

  delete() {
    this.service.delete(this.event.id).subscribe(data => {
      this.deleted.next(true);
    });
  }

}
